
package com.thales.library.adhoc;

import java.util.ArrayList;
import java.util.List;

import org.module.infra.atom.DefaultAtomEntry;
import org.module.infra.general.FeedSyncInfra;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.thales.library.librarydemoapp.R;
import com.thales.library.main.MainActivity;
import com.thales.library.sync.ChatManager;

/**
 * This fragment handles chat related UI which includes a list view for messages
 * and a message entry field with send button.
 */
public class WiFiChatFragment extends SherlockFragment {
	private static final String TAG = "WiFiChatFragment";

    private View view;
    private ChatManager chatManager;
    private TextView chatLine;
    private ListView listView;
    ChatMessageAdapter adapter = null;
    private List<String> items = new ArrayList<String>();
    public FeedSyncInfra feedSyncInfra;
    private List<DefaultAtomEntry> itemsWifiDirect;
    int i;
  
    public void onCreate(Bundle savedInstanceState){
		Log.d(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		MainActivity mActivity = (MainActivity) getActivity();
		feedSyncInfra = mActivity.feedSyncInfra;
		itemsWifiDirect = feedSyncInfra.getListEntry();
	}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        chatLine = (TextView) view.findViewById(R.id.txtChatLine);
        listView = (ListView) view.findViewById(android.R.id.list);
        adapter = new ChatMessageAdapter(getActivity(), android.R.id.text1,
                items);
        i=0;
        listView.setAdapter(adapter);
        view.findViewById(R.id.button1).setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        if (chatManager != null) {
                        	chatManager.write(itemsWifiDirect.get(i).getTitle().toString().getBytes());
                            pushMessage("" + itemsWifiDirect.get(i).getTitle().toString());
                            chatLine.setText("");
                            chatLine.clearFocus();
                            if(i<itemsWifiDirect.size()-1){
                            	i++;
                            }
                             /*for(int i=0;i<itemsWifiDirect.size();i++){
                        		chatManager.write(itemsWifiDirect.get(i).getTitle().toString().getBytes());
                                pushMessage("" + itemsWifiDirect.get(i).getTitle().toString());
                                chatLine.setText("");
                                chatLine.clearFocus();
                        	}*/
                        }
                    }
                });
        return view;
    }

    public interface MessageTarget {
        public Handler getHandler();
    }

    public void setChatManager(ChatManager obj) {
        chatManager = obj;
    }

    public void pushMessage(String readMessage) {
        adapter.add(readMessage);
        adapter.notifyDataSetChanged();
    }

    /**
     * ArrayAdapter to manage chat messages.
     */
    public class ChatMessageAdapter extends ArrayAdapter<String> {

        List<String> messages = null;

        public ChatMessageAdapter(Context context, int textViewResourceId,
                List<String> items) {
            super(context, textViewResourceId, items);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getActivity()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(android.R.layout.simple_list_item_1, null);
            }
            String message = items.get(position);
            if (message != null && !message.isEmpty()) {
                TextView nameText = (TextView) v
                        .findViewById(android.R.id.text1);

                if (nameText != null) {
                    nameText.setText(message);
                    if (message.startsWith("")) {
                        nameText.setTextAppearance(getActivity(),
                                R.style.normalText);
                    } else {
                        nameText.setTextAppearance(getActivity(),
                                R.style.boldText);
                    }
                }
            }
            return v;
        }
    }
}
