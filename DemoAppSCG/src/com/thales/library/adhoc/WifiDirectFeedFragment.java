package com.thales.library.adhoc;

import java.util.ArrayList;

import org.module.infra.atom.DefaultAtomEntry;
import org.module.infra.general.FeedSyncInfra;

import Main.PullToRefreshListView;
import Main.PullToRefreshListView.OnRefreshListener;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.thales.library.librarydemoapp.R;
import com.thales.library.main.ListEntriesSended;
import com.thales.library.main.MainActivity;
import com.thales.library.sync.SyncManager;

public class WifiDirectFeedFragment extends SherlockFragment{
	private static final String TAG = "WifiDirectFeedFragment";
	
	private PullToRefreshListView listView;
	private ArrayList<DefaultAtomEntry> items = new ArrayList<DefaultAtomEntry>();
	private ArrayList<DefaultAtomEntry> itemsWifiDirect = new ArrayList<DefaultAtomEntry>();
	
	public FeedEntryAdapter adapter;
	private SyncManager syncManager;
	public FeedSyncInfra feedSyncInfra;
	
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		MainActivity mActivity = (MainActivity) getActivity();
		feedSyncInfra = mActivity.feedSyncInfra;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment

		View v = inflater.inflate(R.layout.fragment_stream_with_pulldown, null);
		
		listView = (PullToRefreshListView)v.findViewById(R.id.pullToRefreshListView1);
        
        adapter = new FeedEntryAdapter(items, getActivity());
		
        //adapter = new FeedEntryAdapter(feedSyncInfra.getAllEntry(),getActivity());
        listView.setAdapter(adapter);
        
        listView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// Your code to refresh the list contents goes here

				// Make sure you call listView.onRefreshComplete()
				// when the loading is done. This can be done from here or any
				// other place, like on a broadcast receive from your loading
				// service or the onPostExecute of your AsyncTask.

				// For the sake of this sample, the code will pause here to
				// force a delay when invoking the refresh
				/*if(feedSyncInfra.address!=null){
					customAdapter = new CustomListAdapter(getActivity(),
						R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
						new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
						new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container});*/

				//}else{
				//	listView.onRefreshComplete();
				//}
				
				ListEntriesSended mListEntriesSended = new ListEntriesSended(feedSyncInfra.getArrayListEntry());
				
				 if (syncManager != null) {
					 syncManager.write(mListEntriesSended);
				 }
			}
        });
        
        /*listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				DefaultAtomEntry mEntry = feedSyncInfra.getSampleEntry(arg2);
				// TODO Auto-generated method stub
				Log.d(TAG,"getContent: "+mEntry.getContent());
				Log.d(TAG,"getSummary: "+mEntry.getSummary());
				Log.d(TAG,"position: "+arg2);
				
				ItemListSyncDialog iLD = new ItemListSyncDialog();
				iLD.setTitleDialog(mEntry.getTitle());
				iLD.setDescriptionDialog(mEntry.getSummary());
				iLD.setDateDialog(mEntry.getPublished().toGMTString());
				iLD.setContentDialog(mEntry.getContent());
				iLD.setPositionDialog(arg2);
				Bundle extras = new Bundle();
				extras.putParcelable(MainActivity.EXTRA_ITEM, iLD);
				
				DetailEntryFragment mDetailEntryFragment = new DetailEntryFragment();
				mDetailEntryFragment.setArguments(extras);
				
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, mDetailEntryFragment)
				//.addToBackStack(null)
				.commit();
				
				//feedSyncInfra.getAllEntry().get(arg2).getTitle();
				//Toast.makeText(mActivity, viewHolder.title.getText(), Toast.LENGTH_SHORT).show();
				if (mViewHolder.title.getText() != null){
					Toast.makeText(getActivity(), mViewHolder.title.getText(), Toast.LENGTH_SHORT).show();
				}
				
			}
		});*/

        
        return v;
	}
	
	public interface MessageTarget {
        public Handler getHandler();
    }

    public void setSyncManager(SyncManager obj) {
        syncManager = obj;
    }

    public void pushArrayList(ArrayList<DefaultAtomEntry> readArrayList) {
        adapter.clear();
        adapter.addAll(readArrayList);
        adapter.notifyDataSetChanged();
    }
	
	private class FeedEntryAdapter extends ArrayAdapter<DefaultAtomEntry> {

    	private ArrayList<DefaultAtomEntry> feedEntryList;
    	private Context context;

    	public FeedEntryAdapter(ArrayList<DefaultAtomEntry> feedEntryList, Context ctx) {
    		super(ctx, R.layout.atom_list_row, feedEntryList);
    		this.feedEntryList = feedEntryList;
    		this.context = ctx;
    	}

    	public int getCount() {
    		return feedEntryList.size();
    	}

    	public DefaultAtomEntry getItem(int position) {
    		return feedEntryList.get(position);
    	}

    	public long getItemId(int position) {
    		return feedEntryList.get(position).hashCode();
    	}

    	public View getView(int position, View convertView, ViewGroup parent) {
    		View v = convertView;

    		FeedEntryHolder holder = new FeedEntryHolder();

    		// First let's verify the convertView is not null
    		if (convertView == null) {
    			// This a new view we inflate the new layout
    			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			v = inflater.inflate(R.layout.atom_list_row, null);
    			// Now we can fill the layout with the right values
    			TextView titleAtom = (TextView) v.findViewById(R.id.titleAtomRow);
    			TextView dateAtom = (TextView) v.findViewById(R.id.dateAtomRow);

    			holder.title = titleAtom;
    			holder.date = dateAtom;

    			v.setTag(holder);
    		}
    		else 
    			holder = (FeedEntryHolder) v.getTag();

    		DefaultAtomEntry p = feedEntryList.get(position);
    		holder.title.setText(p.getTitle());
    		holder.date.setText("" + p.getPublished().toGMTString());

    		return v;
    	}

    	class FeedEntryHolder {
    		public TextView title;
    		public TextView date;
    	}
    }

}

