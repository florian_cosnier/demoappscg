package com.thales.library.main;

import org.module.debug.DebugLibrary;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.thales.library.librarydemoapp.R;

@SuppressLint("NewApi")
public class DebugFragment extends SherlockFragment{
	
	private static final String TAG = "DebugFragment";

	protected TextView logText = null;

	private DebugLibrary mDebug;
	
	public void onCreate(Bundle savedInstanceState) {  
	    super.onCreate(savedInstanceState);  
	    setHasOptionsMenu(true);  
	}  

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.actionbardebug, menu);
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
	    	switch (item.getItemId()) {
	    		case R.id.pingDebug:
	    			takeAddress();
	    	}
	    	return super.onOptionsItemSelected(item);
	 }
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		
		mDebug = new DebugLibrary(getActivity());
		
		View v = inflater.inflate(R.layout.fragment_debug, null);
		logText = (TextView) v.findViewById(R.id.textViewDebug);
		mDebug.showConfig(logText);
        return v;
	}
	
	public void onResume(){
		super.onResume();
		mDebug.showConfig(logText);
	}
	
	private void takeAddress() {

		View mView = View.inflate( getActivity(), R.layout.set_ping_address_dialog, null);
		final EditText mMessage = ((EditText) mView.findViewById(R.id.pingAddressEditText));
		final InputMethodManager mInputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		mInputMethodManager.restartInput(mView);
		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
		mBuilder.setTitle("Write an address to ping.");
		mBuilder.setPositiveButton("ping...", new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface mDialogInterface, int mWhich) {
				mDebug.testaPing(logText, mMessage.getText().toString().trim());
			}
		});
		mBuilder.setView(mView);
		mBuilder.show();
		return;
	}

}
