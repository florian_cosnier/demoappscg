package com.thales.library.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockFragment;
import com.thales.library.librarydemoapp.R;

/**
 * This fragment gets and save the address of the feed to download
 * 
 * @author flo
 *
 */
@SuppressLint("NewApi")
public class AddFeedFragment extends SherlockFragment{
	static final String TAG = "AddFeedFragment";
	
	public String getUrlStream;
	public Button addFeed;
	public EditText urlStream;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG,"onCreateView");
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_dialog, null);
		
		urlStream = (EditText) v.findViewById(R.id.editTextUrlStream);
		
		addFeed = (Button)v.findViewById(R.id.buttonAddFeed);
		addFeed.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				MainActivity mMainActivity = (MainActivity) getActivity();
				getUrlStream = urlStream.getText().toString();
        		mMainActivity.feedSyncInfra.setAddress(getUrlStream);
			}
		});
		
        return v;
	}
	
	public void onResume(){
		Log.d(TAG,"onResume");
		super.onResume();

		
	}
	
	public void onPause(){
		Log.d(TAG,"onPause");
		super.onPause();

		
	}
}