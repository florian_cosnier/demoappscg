package com.thales.library.main;
import java.io.Serializable;
import java.util.ArrayList;

import org.module.infra.atom.DefaultAtomEntry;


public class ListEntriesSended implements Serializable{
	
	private static final long serialVersionUID = 0L;
	public ArrayList<DefaultAtomEntry> arrayListEntries = new ArrayList<DefaultAtomEntry>();
	
	public  ListEntriesSended(ArrayList<DefaultAtomEntry> arrayListEntries){  
		this.arrayListEntries=arrayListEntries;  
	}
}
