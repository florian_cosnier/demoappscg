package com.thales.library.main;

import com.thales.library.librarydemoapp.R;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.util.Log;
import de.keyboardsurfer.android.widget.crouton.Crouton;

@SuppressLint("NewApi")
public class PrefsFragment extends PreferenceFragment implements
		OnSharedPreferenceChangeListener {
	static final String TAG = "PrefsFragment";

	public static final String KEY_PREF_EXERCISES = "pref_changes";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.d(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.activity_settings);
	}

	public void onPause() {
		super.onPause();
		Log.d(TAG,"onPause");

		PreferenceManager pm = getPreferenceManager();
		SharedPreferences sp = pm.getSharedPreferences();
		sp.registerOnSharedPreferenceChangeListener(this);

		String usernamePrefs = sp.getString("username", "Default NickName");
		boolean checkBoxPrefs = sp.getBoolean("checkBox", false);
		
		boolean checkBoxGPSPrefs = sp.getBoolean("checkBoxGPS", false);

		MainActivity mActivity = (MainActivity) getActivity();
		mActivity.usernameTextView.setText(usernamePrefs);
		
		Log.d("PrefsFragment",usernamePrefs);
		
		if(checkBoxPrefs == true){
        	mActivity.wifiManager.setWifiEnabled(true);
        	Crouton.cancelAllCroutons();
        	
        }else{
        	mActivity.wifiManager.setWifiEnabled(false);
        	/*Crouton.cancelAllCroutons();
        	Crouton croutonTest = new Crouton(getActivity(),"Infrastructure mode", Style.INFO);
        	croutonTest.setConfiguration(Configuration.DEFAULT_INFINITE);
        	croutonTest.show();*/
        }
		
		if(checkBoxGPSPrefs == true){
        	mActivity.location = mActivity.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        	
        }else{
        	
        }
		
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences arg0, String arg1) {
		// TODO Auto-generated method stub	
	}
	
	public void onResume(){
		Log.d(TAG,"onResume");
		super.onResume();
	}
}