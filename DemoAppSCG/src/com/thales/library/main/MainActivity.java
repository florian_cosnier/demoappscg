package com.thales.library.main;

import org.module.geo.GPSTracker;
import org.module.infra.general.FeedSyncInfra;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.thales.library.adhoc.NeighboursFragment;
import com.thales.library.adhoc.NeighboursFragment.DeviceActionListener;
import com.thales.library.adhoc.WiFiChatFragment;
import com.thales.library.adhoc.WiFiDirectBroadcastReceiver;
import com.thales.library.adhoc.WifiDirectFeedFragment;
import com.thales.library.infra.StreamFragment;
import com.thales.library.librarydemoapp.R;
import com.thales.library.sync.DeviceDetailFragment;

@SuppressLint("NewApi")
public class MainActivity extends SherlockFragmentActivity implements View.OnClickListener, DeviceActionListener{
	public static final String TAG = "MainActivity";
	
	public static String EXTRA_ITEM = "ITEM";
	public static String EXTRA_COMMENT = "COMMENT";

	public SharedPreferences prefs;
	public String usernamePrefs;
	public boolean checkBoxPrefs;
	public TextView usernameTextView;
	public TextView positionTextView;

	public Context context;
	
	private  SlidingMenu menuLeft;
	private  SlidingMenu menuRight;
	
	public OnSharedPreferenceChangeListener listener;
	public static final String KEY_PREF_EXERCISES = "pref_changes";
	
	public FeedSyncInfra feedSyncInfra;
	public String getStreamName;
	public String getUrlStream;
	public EditText streamName;
	public EditText urlStream;
	public StreamFragment mStreamFragment;
	public AddFeedFragment mAddFeedFragment;
	public NeighboursFragment mSampleListFragment;
	
	private WifiP2pManager manager;
	private boolean isWifiP2pEnabled = false;
	private boolean retryChannel = false;
	private final IntentFilter intentFilter = new IntentFilter();
	private Channel channel;
	private BroadcastReceiver receiver = null;
	
	public WifiManager wifiManager;
	
	public LocationManager locationManager;
	public Location location;
	public Location myLocation;
	
	public GPSTracker gps;
	
	public static final int MESSAGE_READ = 0x400 + 1;
	public static final int MY_HANDLE = 0x400 + 2;
	public WiFiChatFragment chatFragment;
	public static final int SERVER_PORT = 4545;
	public int launched;
	
	public WifiDirectFeedFragment wifiDirectFragment;
	
	 public void setIsWifiP2pEnabled(boolean isWifiP2pEnabled) {
	        this.isWifiP2pEnabled = isWifiP2pEnabled;
	    }

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get the preference of the user

		prefs = PreferenceManager
				.getDefaultSharedPreferences(MainActivity.this);
		usernamePrefs = prefs.getString("username", "Default NickName");
		checkBoxPrefs = prefs.getBoolean("checkBox", false);
		
		context = this;

		// Initialization of the 2 different SlidingMenu
		
		menuLeft = new SlidingMenu(this);
		menuLeft.setMode(SlidingMenu.LEFT);
		menuLeft.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
		menuLeft.setShadowWidthRes(R.dimen.shadow_width);
		menuLeft.setShadowDrawable(R.drawable.shadow);
		menuLeft.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		menuLeft.setFadeDegree(0.35f);
		menuLeft.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
		menuLeft.setMenu(R.layout.menu_scrollview);
		
		menuRight = new SlidingMenu(this);
        menuRight.setMode(SlidingMenu.RIGHT);
        menuRight.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        menuRight.setShadowWidthRes(R.dimen.shadow_width);
        menuRight.setShadowDrawable(R.drawable.shadowright);
        menuRight.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menuRight.setFadeDegree(0.35f);
        menuRight.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        menuRight.setMenu(R.layout.menu_frame);
        
        // Initialization of the different connection manager (WiFi, location)
		
		wifiManager = (WifiManager)this.context.getSystemService(Context.WIFI_SERVICE);
		positionTextView = (TextView)findViewById(R.id.PositionMenu);
		locationManager = (LocationManager)getSystemService(LOCATION_SERVICE);
		location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		Location locationNet = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		myLocation = null;
		
		intentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        intentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        manager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        channel = manager.initialize(this, getMainLooper(), null);
        
        receiver = new WiFiDirectBroadcastReceiver(manager, channel, this);
        registerReceiver(receiver, intentFilter);
        
        //
		
		usernameTextView = (TextView)findViewById(R.id.UsernameMenu);
        usernameTextView.setOnClickListener(this);
        findViewById(R.id.PositionMenu).setOnClickListener(this);
        
        // Initialization of the different fragments
        
        feedSyncInfra = new FeedSyncInfra(context, this); 
		mSampleListFragment = new NeighboursFragment();
        mStreamFragment = new StreamFragment();
        mAddFeedFragment = new AddFeedFragment();
        chatFragment = new WiFiChatFragment();
        wifiDirectFragment = new WifiDirectFeedFragment();
        
        // Assign a fragment for each item of the menu
        
        findViewById(R.id.AddFeed).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menuLeft.toggle();
				
				getFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new EmptyFragment())
				.commit();
				
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, mAddFeedFragment)
				.commit();
			}
		});
        findViewById(R.id.HomeMenu).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG,"OnHomeMenu");
				// TODO Auto-generated method stub
				menuLeft.toggle();

				getFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new EmptyFragment())
				.commit();
				
				
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, mStreamFragment)
				.commit();
			}
		});
        findViewById(R.id.ChatMenu).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menuLeft.toggle();
				getFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new EmptyFragment())
				.commit();
				
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, chatFragment)
				.commit();
			}
        });
        findViewById(R.id.MapMenu).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d(TAG,"OnHomeMenu");
				// TODO Auto-generated method stub
				menuLeft.toggle();
			}
		});
        findViewById(R.id.NotificationMenu).setOnClickListener(this);
        findViewById(R.id.SettingsMenu).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menuLeft.toggle();
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new EmptySupportFragment())
				.commit();
				
				getFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new PrefsFragment())
				.commit();
			}
		});
        findViewById(R.id.DebugMenu).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				menuLeft.toggle();
				getFragmentManager().beginTransaction()
				.replace(R.id.main_frame,new EmptyFragment())
				.commit();
				
				getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, new DebugFragment())
				.commit();
			}
		});
        
        findViewById(R.id.HelpMenu).setOnClickListener(this);
        
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        StreamFragment fragment = new StreamFragment();
        fragmentTransaction.add(R.id.main_frame, fragment);
        fragmentTransaction.commit();


	}

	public void onResume(){
    	super.onResume();
		Log.d(TAG,"onResume");
    	usernamePrefs = prefs.getString("username", "Username");
        checkBoxPrefs = prefs.getBoolean("checkBox", false);
        usernameTextView.setText(usernamePrefs);
        
        Log.d(TAG,"prefs test: "+checkBoxPrefs);
    }
	
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.main, menu);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
    		ActionBar actionBar = getSupportActionBar();
    		actionBar.setDisplayHomeAsUpEnabled(true);
    	}
        return true;
    }
    
    // Assign some functions for each items of the action bar
    // For the social item, it launches the discovery of the neighbours by Wifi-Direct
    
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
    		case android.R.id.home:
    			menuLeft.toggle();
    			break;

    		case R.id.menu_social:
    			menuRight.toggle();
    			if (!isWifiP2pEnabled) {
                    Toast.makeText(MainActivity.this, "WARNING",
                            Toast.LENGTH_SHORT).show();
                    return true;
                }
    			final NeighboursFragment fragment = (NeighboursFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.frag_list);
                fragment.onInitiateDiscovery();
                manager.discoverPeers(channel, new WifiP2pManager.ActionListener() {

                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "Discovery Initiated",
                                Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int reasonCode) {
                        Toast.makeText(MainActivity.this, "Discovery Failed : " + reasonCode,
                                Toast.LENGTH_SHORT).show();
                    }
                });
    	}
    	
    	return super.onOptionsItemSelected(item);
    }
    
    public void onClick(View v) {
		menuLeft.toggle();
  }
    
    public void showDetails(WifiP2pDevice device) {
    	Log.d(TAG,"showDetails");
        DeviceDetailFragment fragment = (DeviceDetailFragment) getSupportFragmentManager()
                .findFragmentById(R.id.frag_detail);
        fragment.showDetails(device);
    }
 
    
    /*public boolean handleMessage(Message msg) {
        switch (msg.what) {
            case MESSAGE_READ:
                byte[] readBuf = (byte[]) msg.obj;
                // construct a string from the valid bytes in the buffer
                String readMessage = new String(readBuf, 0, msg.arg1);
                Log.d(TAG, readMessage);
                (wifiDirectFragment).pushMessage("Buddy: " + readMessage);
                break;

            case MY_HANDLE:
                Object obj = msg.obj;
                (chatFragment).setChatManager((ChatManager) obj);

        }
        return true;
    }*/
   
   public void connect(WifiP2pConfig config) {
       manager.connect(channel, config, new ActionListener() {

           @Override
           public void onSuccess() {
               // WiFiDirectBroadcastReceiver will notify us. Ignore for now.
           }

           @Override
           public void onFailure(int reason) {
               Toast.makeText(MainActivity.this, "Connect failed. Retry.",
                       Toast.LENGTH_SHORT).show();
           }
       });
   }

   public void disconnect() {
       final DeviceDetailFragment fragment = (DeviceDetailFragment) getSupportFragmentManager()
               .findFragmentById(R.id.frag_detail);
       fragment.resetViews();
       manager.removeGroup(channel, new ActionListener() {

           @Override
           public void onFailure(int reasonCode) {
               Log.d(TAG, "Disconnect failed. Reason :" + reasonCode);

           }

           @Override
           public void onSuccess() {
               fragment.getView().setVisibility(View.GONE);
           }

       });
   }
   
   public void resetData() {
       NeighboursFragment fragmentList = (NeighboursFragment) getSupportFragmentManager()
               .findFragmentById(R.id.frag_list);
       DeviceDetailFragment fragmentDetails = (DeviceDetailFragment) getSupportFragmentManager()
               .findFragmentById(R.id.frag_detail);
       if (fragmentList != null) {
           fragmentList.clearPeers();
       }
       if (fragmentDetails != null) {
           fragmentDetails.resetViews();
       }
   }

	@Override
	public void cancelDisconnect() {
		// TODO Auto-generated method stub
		
	}
	
	 /*public void onConnectionInfoAvailable(WifiP2pInfo p2pInfo) {
	        Thread handler = null;
	        //
	         // The group owner accepts connections using a server socket and then spawns a
	         //client socket for every client. This is handled by {@code
	         // GroupOwnerSocketHandler}
	         //

	        if (p2pInfo.isGroupOwner) {
	            Log.d(TAG, "Connected as group owner");
	            stry {
	                handler = new SyncGroupOwnerSocketHandler(
	                        ((MessageTarget) this).getHandler());
	                handler.start();
	            } catch (IOException e) {
	                Log.d(TAG,
	                        "Failed to create a server thread - " + e.getMessage());
	                return;
	            }
	        } else {
	            Log.d(TAG, "Connected as peer");
	            handler = new SyncClientSocketHandler(
	                    ((MessageTarget) this).getHandler(),
	                    p2pInfo.groupOwnerAddress);
	            handler.start();
	        }
	    }*/
	
}


