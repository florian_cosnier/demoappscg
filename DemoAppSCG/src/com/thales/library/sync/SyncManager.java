
package com.thales.library.sync;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;

import org.module.infra.atom.DefaultAtomEntry;

import android.os.Handler;
import android.util.Log;

import com.thales.library.main.ListEntriesSended;
import com.thales.library.main.MainActivity;

/**
 * Handles reading and writing of messages with socket buffers. Uses a Handler
 * to post messages to UI thread for UI updates.
 */
public class SyncManager implements Runnable {

    private Socket socket = null;
    private Handler handler;

    public SyncManager(Socket socket, Handler handler) {
        this.socket = socket;
        this.handler = handler;
    }

    private InputStream iStream;
    private OutputStream oStream;
    private ObjectOutputStream ooStream;
    private ObjectInputStream oiStream;
    private static final String TAG = "SyncManager";

    @Override
    public void run() {
        try {

            iStream = socket.getInputStream();
            oStream = socket.getOutputStream();
            ooStream = new ObjectOutputStream(oStream);
            oiStream = null;
            byte[] buffer = new byte[1024];
            Object obj = null;
            int bytes;
            ArrayList<DefaultAtomEntry> arrayListEntries = new ArrayList<DefaultAtomEntry>();
            ListEntriesSended mListEntriesSended = new ListEntriesSended(arrayListEntries);
            /** Object list... res **/
            handler.obtainMessage(DeviceDetailFragment.MY_HANDLE, this)
                    .sendToTarget();

            while (true) {
                try {
                    // Read from the InputStream
                	//ooStream.writeObject(mListEntriesSended);
                    //ooStream.flush();
                    oiStream = new ObjectInputStream(iStream);
                    bytes = iStream.read(buffer);
                    if (bytes == -1) {
                        break;
                    }
                	try {
                		mListEntriesSended = (ListEntriesSended)oiStream.readObject();
                		if (mListEntriesSended!=null){Log.d(MainActivity.TAG,"Test ="+mListEntriesSended.arrayListEntries.size());}  
                		handler.obtainMessage(DeviceDetailFragment.MESSAGE_READ,
                        		mListEntriesSended).sendToTarget();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
                    /**if (res == null) {
                        break;
                    }**/

                    /** Log.d(TAG, "Size entries:" + res.size()); **/
                    
                } catch (IOException e) {
                    Log.e(TAG, "disconnected", e);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void write(ListEntriesSended res) {
        try {
            ooStream.writeObject(res);
            ooStream.flush();
        } catch (IOException e) {
            Log.e(TAG, "Exception during write", e);
        }
    }

}
