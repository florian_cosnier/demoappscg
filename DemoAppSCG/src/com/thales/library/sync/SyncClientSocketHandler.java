
package com.thales.library.sync;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

import android.os.Handler;
import android.util.Log;

import com.thales.library.main.MainActivity;

public class SyncClientSocketHandler extends Thread {

    private static final String TAG = "SyncClientSocketHandler";
    private Handler handler;
    private SyncManager sync;
    private InetAddress mAddress;

    public SyncClientSocketHandler(Handler handler, InetAddress groupOwnerAddress) {
        this.handler = handler;
        this.mAddress = groupOwnerAddress;
    }

    @Override
    public void run() {
        Socket socket = new Socket();
        try {
            socket.bind(null);
            socket.connect(new InetSocketAddress(mAddress.getHostAddress(),
                    MainActivity.SERVER_PORT), 5000);
            Log.d(TAG, "Launching the I/O handler");
            sync = new SyncManager(socket, handler);
            new Thread(sync).start();
        } catch (IOException e) {
            e.printStackTrace();
            try {
                socket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }
    }

    public SyncManager getSync() {
        return sync;
    }

}
