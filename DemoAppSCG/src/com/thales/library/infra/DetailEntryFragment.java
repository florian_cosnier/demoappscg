package com.thales.library.infra;

import java.util.List;

import org.module.infra.general.FeedSyncInfra;
import org.module.widget.dialog.CommentListItem;
import org.module.widget.dialog.ItemListSyncDialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.text.Html.ImageGetter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.thales.library.librarydemoapp.R;
import com.thales.library.main.MainActivity;

public class DetailEntryFragment extends SherlockFragment{
	
	private ItemListSyncDialog iLD;
	private String titleExtras;
	private String descriptionExtras;
	private String dateExtras;
	private String contentExtras;
	private int positionExtras;
	 
	private FeedSyncInfra syncFeedInfra;
	private List<CommentListItem> commentsList;
	private CommentListFragmentAdapter mCLA;
	
	public long idEntry;
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setHasOptionsMenu(true); 
	    Bundle arguments = getArguments();
	    MainActivity mActivity = (MainActivity) getActivity();
	    syncFeedInfra = mActivity.feedSyncInfra; 
	    if (arguments == null)
	        Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
	    else
	    	{
			iLD = arguments.getParcelable(MainActivity.EXTRA_ITEM);
			titleExtras = iLD.getTitleDialog();
			descriptionExtras = iLD.getDescriptionDialog();
			dateExtras = iLD.getDateDialog();
			contentExtras = iLD.getContentDialog();
			positionExtras = iLD.getPositionDialog();
			
			Log.d("DetailEntryFragment","titleExtras: "+titleExtras);
			Log.d("DetailEntryFragment","descriptionExtras: "+descriptionExtras);
			Log.d("DetailEntryFragment","dateExtras: "+dateExtras);
			Log.d("DetailEntryFragment","contentExtras: "+contentExtras);
			
			idEntry = syncFeedInfra.getIdEntry(positionExtras);
		}
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_detail_entry, null);
		
		
		final WebView webview = (WebView) v.findViewById(R.id.webViewDialog);
		 	webview.getSettings().setJavaScriptEnabled(true);
		
   		 // From http://alex.tapmania.org/2010/11/html5-cache-android-webview.html
   		 webview.setWebChromeClient(new WebChromeClient() {
   		      @Override
   		      public void onReachedMaxAppCacheSize(long spaceNeeded, long totalUsedQuota,
   		                   WebStorage.QuotaUpdater quotaUpdater)
   		      {
   		            quotaUpdater.updateQuota(spaceNeeded * 2);
   		      }
   		});
		 
   		 webview.getSettings().setDomStorageEnabled(true);
   		 webview.getSettings().setAppCacheMaxSize(1024*1024*64);
   		 webview.getSettings().setAppCachePath("/data/data/fr.anr.crowd/cache");
   		 webview.getSettings().setAllowFileAccess(true);
   		 webview.getSettings().setAppCacheEnabled(true);
   		 webview.setBackgroundColor(0x00000000);
   		 String message ="<font color='black'>"+contentExtras+"</font>";
   		 webview.loadData(message, "text/html", "utf8");

        TextView descriptionTV = (TextView) v.findViewById(R.id.descriptionDialog);
        TextView dateTV = (TextView) v.findViewById(R.id.dateDialog);
        ListView listViewComment = (ListView) v.findViewById(R.id.listDialog);
        descriptionTV.setText(descriptionExtras);
        dateTV.setText(dateExtras);
        
		commentsList = syncFeedInfra.getCommentsList(positionExtras);
		Log.d(MainActivity.TAG,"comments size: "+commentsList.size());
		
		mCLA = new CommentListFragmentAdapter(getActivity().getApplicationContext(), commentsList);
        listViewComment.setAdapter(mCLA);
        setListViewHeightBasedOnChildren(listViewComment);
        mCLA.notifyDataSetChanged();
		
        return v;
	}
	
	public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        int desiredWidth = MeasureSpec.makeMeasureSpec(listView.getWidth(), MeasureSpec.AT_MOST);
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
	}
	
	private class CommentListFragmentAdapter extends BaseAdapter {

		List<CommentListItem> listItem;
		LayoutInflater inflater;
		private Context context;
		private static final String TAG ="CommentListFragmentAdapter";
		
		public CommentListFragmentAdapter(Context context,List<CommentListItem> listItem) {
			inflater = LayoutInflater.from(context);
			this.listItem = listItem;
			this.context = context;
		}
		
		@Override
		public int getCount() {
			return listItem.size();
		}

		@Override
		public Object getItem(int position) {
			return listItem.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		
		private class ViewHolderComment{
			TextView author;
			TextView comments;
			TextView date;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolderComment holder = new ViewHolderComment();
			
			if(convertView == null) {
				holder = new ViewHolderComment();
				LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				//convertView = inflater.inflate(R.layout.listview_comment_item, null);
				convertView = inflater.inflate(R.layout.listview_comment_item, null);
				holder.author = (TextView)convertView.findViewById(R.id.textViewCommentAuthorDialog);
				holder.comments = (TextView)convertView.findViewById(R.id.textViewCommentContentDialog);
				holder.date = (TextView)convertView.findViewById(R.id.textViewCommentDateDialog);
				convertView.setTag(holder);
			} 
			
			holder.author.setText(listItem.get(position).getAuthor());
			holder.comments.setText(listItem.get(position).getComments());
			holder.date.setText(listItem.get(position).getDate().toGMTString());
			
			holder.comments.setText(Html.fromHtml(listItem.get(position).getComments(), imgGetter, null));
			
			return convertView;
		}
		
		 private ImageGetter imgGetter = new ImageGetter() {

			 public Drawable getDrawable(String source) {
	             Drawable drawable = null;
	             drawable = Drawable.createFromPath(source);  // Or fetch it from the URL
	             // Important
	             drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable
	                           .getIntrinsicHeight());
	             return drawable;
	       }
		 };
	}
	
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.actionbardetailentry, menu);
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
	    	switch (item.getItemId()) {
	    		case R.id.add_comment:
	    			Bundle extras = new Bundle();
					extras.putLong(MainActivity.EXTRA_COMMENT, idEntry);
					
					AddCommentFragment mAddCommentFragment = new AddCommentFragment();
					mAddCommentFragment.setArguments(extras);
					
					getActivity().getSupportFragmentManager().beginTransaction()
					.replace(R.id.main_frame, mAddCommentFragment)
					.commit();		
	    	}
	    	return super.onOptionsItemSelected(item);
	 }

}