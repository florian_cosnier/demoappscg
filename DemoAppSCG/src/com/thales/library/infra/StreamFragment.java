package com.thales.library.infra;

import java.util.ArrayList;
import java.util.Date;

import org.module.adhoc.general.AdhocCreation;
import org.module.infra.atom.DefaultAtomEntry;
import org.module.infra.database.DataBaseSchema;
import org.module.infra.general.FeedEntry;
import org.module.infra.general.FeedSyncInfra;
import org.module.infra.general.LoadingFeedTask;
import org.module.widget.dialog.ItemListSyncDialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.thales.library.librarydemoapp.R;
import com.thales.library.main.MainActivity;

import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@SuppressLint("NewApi")
public class StreamFragment extends SherlockFragment{
	private static final String TAG = "StreamFragment";
	
	//private PullToRefreshListView listView;
	private ListView simpleListView;
	private ArrayList<FeedEntry> items = new ArrayList<FeedEntry>();
	private ArrayList<DefaultAtomEntry> itemsWifiDirect = new ArrayList<DefaultAtomEntry>();
	
	public static final long MINUTE=60;
	public static final long HOUR=3600;
	public static final long DAY=24*HOUR;
	public static final long WEEK=7*DAY;
	
	public FeedEntryAdapter adapter;
	public CustomListAdapter customAdapter;
	
	private static final int OK_CANCEL_DIALOG = 0;
	private boolean ADHOC_MODE = false;
	
	public TextView usernameTextView;
	
	
	public FeedSyncInfra feedSyncInfra;
	private AdhocCreation mAdhocCreation = null;
	private MainActivity mActivity;
	
	public void onCreate(Bundle savedInstanceState){
		Log.d(TAG,"onCreate");
		super.onCreate(savedInstanceState);
		//initList();
		mAdhocCreation = new AdhocCreation(getActivity()); 
		setHasOptionsMenu(true);  
		MainActivity mActivity = (MainActivity) getActivity();
		feedSyncInfra = mActivity.feedSyncInfra;
		 
	}
	
	public void onResume(){
		Log.d(TAG,"onResume");
		super.onResume();
		setHasOptionsMenu(true);  
		MainActivity mActivity = (MainActivity) getActivity();
		feedSyncInfra = mActivity.feedSyncInfra; 
		
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.fragment_stream_with_simple_listview, null);
		
		//listView = (PullToRefreshListView)v.findViewById(R.id.pullToRefreshListView1);
		simpleListView = (ListView)v.findViewById(R.id.listViewSimple);
        
        adapter = new FeedEntryAdapter(items, getActivity());
		
        //adapter = new FeedEntryAdapter(feedSyncInfra.getAllEntry(),getActivity());
        simpleListView.setAdapter(adapter);
        
        if(items.size()==0 && feedSyncInfra.address!=null && feedSyncInfra.getListEntry().size() == 0){
        	customAdapter = new CustomListAdapter(getActivity(),
					R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
					new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
					new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container}); 
        	
        	Crouton croutonTest = new Crouton(getActivity(),"Loading entries", Style.CONFIRM);
        	croutonTest.setConfiguration(Configuration.DEFAULT_INFINITE);
        	croutonTest.show();
        	//Crouton.makeText(getActivity(), "Loading entries",Style.CONFIRM).show();*/
        	//Crouton
        	new LoadingFeedTask().execute(feedSyncInfra,simpleListView,customAdapter);
        }else if(items.size()==0 && feedSyncInfra.address!=null  && feedSyncInfra.getListEntry().size()!=0){
        	customAdapter = new CustomListAdapter(getActivity(),
					R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
					new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
					new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container});
        	
        	simpleListView.setAdapter(customAdapter);
        	customAdapter.notifyDataSetChanged();
        }
        
        /*listView.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// Your code to refresh the list contents goes here

				// Make sure you call listView.onRefreshComplete()
				// when the loading is done. This can be done from here or any
				// other place, like on a broadcast receive from your loading
				// service or the onPostExecute of your AsyncTask.

				// For the sake of this sample, the code will pause here to
				// force a delay when invoking the refresh
				if(feedSyncInfra.address!=null){
					customAdapter = new CustomListAdapter(getActivity(),
						R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
						new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
						new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container});
					
//					try {
//					if(mAdhocCreation.getMode() == feedSyncInfra.INFRA){
						new LoadingFeedTask().execute(feedSyncInfra,listView,customAdapter);
//					}
//					else{
//						listView.onRefreshComplete();
//					}
//				} catch (RemoteException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
				}else{
					listView.onRefreshComplete();
				}
				

				
//				listView.postDelayed(new Runnable() {
//
//					@Override
//					public void run() {
//						listView.onRefreshComplete();
//					}
//				}, 2000);
			}
		});*/
        
        
        simpleListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				DefaultAtomEntry mEntry = feedSyncInfra.getSampleEntry(arg2);
				// TODO Auto-generated method stub
				Log.d(TAG,"getContent: "+mEntry.getContent());
				Log.d(TAG,"getSummary: "+mEntry.getSummary());
				Log.d(TAG,"position: "+arg2);
				
				ItemListSyncDialog iLD = new ItemListSyncDialog();
				iLD.setTitleDialog(mEntry.getTitle());
				iLD.setDescriptionDialog(mEntry.getSummary());
				iLD.setDateDialog(mEntry.getPublished().toGMTString());
				iLD.setContentDialog(mEntry.getContent());
				iLD.setPositionDialog(arg2);
				Bundle extras = new Bundle();
				extras.putParcelable(MainActivity.EXTRA_ITEM, iLD);
				
				DetailEntryFragment mDetailEntryFragment = new DetailEntryFragment();
				mDetailEntryFragment.setArguments(extras);
				
				getActivity().getSupportFragmentManager().beginTransaction()
				.replace(R.id.main_frame, mDetailEntryFragment)
				//.addToBackStack(null)
				.commit();
				
				//feedSyncInfra.getAllEntry().get(arg2).getTitle();
				//Toast.makeText(mActivity, viewHolder.title.getText(), Toast.LENGTH_SHORT).show();
				/*if (mViewHolder.title.getText() != null){
					Toast.makeText(getActivity(), mViewHolder.title.getText(), Toast.LENGTH_SHORT).show();
				}*/
				
			}
		});
		
        return v;
	}
    
    /*private void initList() {
        // We populate the planets 
    	DefaultAtomEntry flo =new DefaultAtomEntry();
    	flo.setTitle("Title");
        items.add(flo);
    }*/
	 
    private class FeedEntryAdapter extends ArrayAdapter<FeedEntry> {

    	private ArrayList<FeedEntry> feedEntryList;
    	private Context context;

    	public FeedEntryAdapter(ArrayList<FeedEntry> feedEntryList, Context ctx) {
    		super(ctx, R.layout.atom_list_row, feedEntryList);
    		this.feedEntryList = feedEntryList;
    		this.context = ctx;
    	}

    	public int getCount() {
    		return feedEntryList.size();
    	}

    	public FeedEntry getItem(int position) {
    		return feedEntryList.get(position);
    	}

    	public long getItemId(int position) {
    		return feedEntryList.get(position).hashCode();
    	}

    	public View getView(int position, View convertView, ViewGroup parent) {
    		View v = convertView;

    		FeedEntryHolder holder = new FeedEntryHolder();

    		// First let's verify the convertView is not null
    		if (convertView == null) {
    			// This a new view we inflate the new layout
    			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    			v = inflater.inflate(R.layout.atom_list_row, null);
    			// Now we can fill the layout with the right values
    			TextView titleAtom = (TextView) v.findViewById(R.id.titleAtomRow);
    			TextView dateAtom = (TextView) v.findViewById(R.id.dateAtomRow);

    			holder.title = titleAtom;
    			holder.date = dateAtom;

    			v.setTag(holder);
    		}
    		else 
    			holder = (FeedEntryHolder) v.getTag();

    		FeedEntry p = feedEntryList.get(position);
    		holder.title.setText(p.getTitle());
    		holder.date.setText("" + p.getDate());

    		return v;
    	}

    	class FeedEntryHolder {
    		public TextView title;
    		public TextView date;
    	}
    }
    
    private class CustomListAdapter extends SimpleCursorAdapter{
    	
    	private LayoutInflater mInflater;
		private Cursor cur;
		private final static int WIDTH=200;
		private final static int HEIGHT=200;

		public CustomListAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to) {
			super(context, layout, c, from, to);
			//this.layout = layout;
			this.mInflater=LayoutInflater.from(context);
			this.cur=c;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder;
			this.cur.moveToPosition(position);
			
			viewHolder = new ViewHolder();

			convertView = this.mInflater.inflate(R.layout.atom_list_row, null);
			if(convertView == null){
				convertView = this.mInflater.inflate(R.layout.empty_atom_list_row, null);
				convertView.setVisibility(View.GONE);
				convertView.setTag(viewHolder);
			}
	
			viewHolder.title = (TextView)convertView.findViewById(R.id.titleAtomRow);
			viewHolder.date = (TextView)convertView.findViewById(R.id.dateAtomRow);
			viewHolder.title.setText(this.cur.getString(this.cur.getColumnIndex(DataBaseSchema.EntrySchema.COLUMN_TITLE)));  
			viewHolder.date.setText(returnTimeDiff(this.cur.getDouble(this.cur.getColumnIndex(DataBaseSchema.EntrySchema.COLUMN_PUBDATE))));

			viewHolder = (ViewHolder) convertView.getTag();
			
			return convertView;
		}
    }
    
    class ViewHolder{
		ImageView img;
		TextView title;
		TextView description;
		TextView date;
		TextView pos;
	}
    
    private final String returnTimeDiff(double time){
		Date actualTime= new Date();

		double diff=actualTime.getTime()-time;
		int diff1=(int) (diff/1000);
		if (diff1<MINUTE){
			return("1 minute ago");
		}
		else if(diff1<HOUR){
			return((int)(diff1/60)+" minutes ago");
		}

		else if (diff1<DAY){
			return((int)(diff1/60/60)+" hours ago");
		}

		else if (diff1<WEEK){
			return((int)(diff1/60/60/24)+" days ago");
		}
		return("More than one week ago");
	}
    
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.actionbarfeedfragment, menu);
	}
	
	 public boolean onOptionsItemSelected(MenuItem item) {
	    	switch (item.getItemId()) {
	    		case R.id.refresh_feed:
	    			
	    			if(feedSyncInfra.address!=null && feedSyncInfra.getListEntry().size() != 0){
	    	        	customAdapter = new CustomListAdapter(getActivity(),
	    						R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
	    						new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
	    						new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container}); 
	    	        	
	    	        	Crouton croutonTest = new Crouton(getActivity(),"Loading entries", Style.CONFIRM);
	    	        	croutonTest.setConfiguration(Configuration.DEFAULT_INFINITE);
	    	        	croutonTest.show();
	    	        	//Crouton.makeText(getActivity(), "Loading entries",Style.CONFIRM).show();*/
	    	        	//Crouton
	    	        	new LoadingFeedTask().execute(feedSyncInfra,simpleListView,customAdapter);
	    	        }
	    			
	    			/*customAdapter = new CustomListAdapter(getActivity(),
	    					R.layout.atom_list_row,feedSyncInfra.getCursorEntries(), 
	    					new String[] {feedSyncInfra.getDbColumnTitle(),feedSyncInfra.getDbColumnAuthorName(), feedSyncInfra.getDbColumnRead() },
	    					new int[]{R.id.titleAtomRow,R.id.dateAtomRow, R.id.row_container});  
	    			Crouton croutonTest = new Crouton(getActivity(),"Loading entries", Style.CONFIRM);
	            	croutonTest.setConfiguration(Configuration.DEFAULT_INFINITE);
	            	croutonTest.show();
	            	new LoadingFeedTask().execute(feedSyncInfra,simpleListView,customAdapter);*/
					
	    	}
	    	return super.onOptionsItemSelected(item);
	 }
}
