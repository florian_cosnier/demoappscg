package com.thales.library.infra;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.module.infra.atom.AtomPerson;
import org.module.infra.atom.DefaultAtomEnclosure;
import org.module.infra.atom.DefaultAtomEntry;
import org.module.infra.atom.DefaultAtomFeed;
import org.module.infra.atom.DefaultAtomLink;
import org.module.infra.atom.DefaultAtomPerson;
import org.module.infra.database.DataBaseAdapter;
import org.module.infra.database.DataBaseSchema;
import org.module.infra.general.FeedSyncInfra;
import org.module.infra.sync.ServerDialog;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.thales.library.librarydemoapp.R;
import com.thales.library.main.MainActivity;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * This fragment adds comment to an entry
 * @author flo
 *
 */
public class AddCommentFragment extends SherlockFragment{
	
	private static final String TAG = "AddCommentFragment";
	
	public long idEntry;
	private FeedSyncInfra syncFeedInfra;
	
	static final int ATOM_CREATE_COMMENT=99;
	protected static final int SELECT_IMAGE = 15;
	protected static final int CAMERA = 18;
	
	private String cameraPhotoPath;
	private DataBaseAdapter mDba; 
	private ImageView img;
	DefaultAtomEnclosure enc;
	private DefaultAtomFeed comFeed;
	MainActivity mainActivity;
	DefaultAtomEntry newEntry;
	
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    mainActivity = (MainActivity) getActivity();
	    syncFeedInfra = mainActivity.feedSyncInfra;
	    mDba = syncFeedInfra.getdBa();

	    Bundle arguments = getArguments(); 
	    
	    if (arguments == null)
	        Toast.makeText(getActivity(), "Arguments is NULL", Toast.LENGTH_LONG).show();
	    else
	    	{
	    	idEntry=arguments.getLong(MainActivity.EXTRA_COMMENT);
	    	Log.d(TAG,"id: "+idEntry);
		}
	    
	    Cursor cur=mDba.mDb.query(DataBaseSchema.EntrySchema.TABLE_NAME, new String[]{DataBaseSchema.EntrySchema.COLUMN_FEED_ID}, DataBaseSchema.EntrySchema._ID+"=?", new String[]{String.valueOf(idEntry)}, null, null, null);
		cur.moveToFirst();
		cur.close();

	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View v = inflater.inflate(R.layout.add_comment, null);
		
		final EditText editUser=(EditText)v.findViewById(R.id.comment_user);
		final EditText editComment=(EditText)v.findViewById(R.id.comment_body);
			
		Button submitComment=(Button) v.findViewById(R.id.add_comment_activity);
		submitComment.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				try {
					// add the comment locally and upload it in the server if the WIFI is Infrastructure
					addComment(editUser.getText().toString(),editComment.getText().toString(),idEntry);
				
					//if it is ADHOC send the message in broadcast

				}catch (RemoteException e) {
					Log.e(TAG,"RemoteException!!!!!");
					e.printStackTrace();
				}
			}
		});
		
		Button add_pic=(Button) v.findViewById(R.id.select_pics);
		add_pic.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
				startActivityForResult(intent, SELECT_IMAGE);

			}
		});
		
		Button camera=(Button) v.findViewById(R.id.camera);
		camera.setOnClickListener(new OnClickListener(){
			public void onClick(View v)	{ 
				Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
				intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File("/sdcard/download/tmp")));
				startActivityForResult(intent, CAMERA);
			}
		});	
		
		img=(ImageView) v.findViewById(R.id.added_img);
		
        return v;
	}
	
	private void addComment(String title,String comment, long referToId) throws RemoteException{
		(new PostThread(title,comment,referToId)).execute(); 
	}
	
	// Thread to post comments
	class PostThread extends AsyncTask<Void, Void, Boolean>{


		long referToId;
		String title;
		String comment;
		int mode;

		PostThread(String title,String comment, long referToId){
			super();
			this.referToId=referToId;
			this.title=title;
			this.comment=comment;
		}

		@Override
		protected Boolean doInBackground(Void...params) {
			String android_id = Secure.getString(getActivity().getContentResolver(), Secure.ANDROID_ID); 

			mode=syncFeedInfra.getMode();

			if (android_id == null) { 
				Log.d(TAG,"NULL") ;
			} else { 

				if(!mDba.isOpen())
					mDba.open();

				Date date=new Date();
				//Timestamp time= new Timestamp(date.getTime());//unique timestamp of creation

				Log.d(TAG,"addComment("+referToId+")");

				DefaultAtomEntry entry=mDba.getEntry(referToId);
				
				Log.d(TAG,entry.toString());
				Log.d(TAG,entry.getEntryId());
				newEntry= new DefaultAtomEntry();
				List<AtomPerson> persons=new ArrayList<AtomPerson>();
				DefaultAtomPerson person=new DefaultAtomPerson();

				person.setName(title);
				persons.add(person);


				if ( cameraPhotoPath!=null){// i add the local location in the message body
					comment=comment+"<p>  <img align= \"middle\" src=\""+cameraPhotoPath+"\"> ";
				}
				
				newEntry.setContent(comment);
				newEntry.setTitle(title);
				newEntry.setAuthors(persons);
				newEntry.setReferTo(referToId);
				newEntry.setPublished(date);
				newEntry.setEntryId(android_id+date.getTime());
				
				long commentId;
				Cursor cur=mDba.mDb.query(DataBaseSchema.FeedSchema.TABLE_NAME, new String[]{DataBaseSchema.FeedSchema._ID,DataBaseSchema.FeedSchema.COLUMN_URL}, DataBaseSchema.FeedSchema.COLUMN_REFER_TO_ENTRY+"=?",new String[]{String.valueOf(referToId)},null, null, null);
				if (cur.moveToFirst()){
					Log.d(TAG,cur.getString(cur.getColumnIndex(DataBaseSchema.FeedSchema._ID)));
					newEntry.setParentId(cur.getString(cur.getColumnIndex(DataBaseSchema.FeedSchema._ID)));
					commentId=mDba.insertEntry(mDba.mDb, newEntry,cur.getLong(cur.getColumnIndex(DataBaseSchema.FeedSchema._ID)), mode);
				}
				//if the replies feed does not exist
				else{

					comFeed= new DefaultAtomFeed();
					comFeed.setFeedId(entry.getEntryId()+"/feed/atom");
					comFeed.setTitle(entry.getEntryId()+"/feed/atom");

					newEntry.setParentId(String.valueOf( mDba.insertFeed(mDba.mDb, comFeed, referToId)));

					DefaultAtomLink newLink = new DefaultAtomLink();
					newLink.setRel("replies");
					newLink.setHref(entry.getEntryId()+"/feed/atom");
					newLink.setType("application/atom+xml");

					newEntry.addLink(newLink);
					mDba.insertLink(mDba.mDb, newLink, referToId, mode);
					commentId= mDba.insertEntry(mDba.mDb, newEntry,Long.valueOf(newEntry.getParentId()), mode);
				}

				//if i have a photo to add---> new enclosure		
				if(mode==DataBaseSchema.INFRA){
					String address=cur.getString(cur.getColumnIndex(DataBaseSchema.FeedSchema.COLUMN_URL));

					if (newEntry.hasEnclosures()){
						int code=ServerDialog.executeHttpPostData(new File(cameraPhotoPath),address, newEntry,enc).getStatusLine().getStatusCode();
						if(code==201||code==204) {
							mDba.insertEnclosure(mDba.mDb,enc , commentId,DataBaseSchema.INFRA);
//							BftService.notifyFeedChanged();
						}
						else Log.d(TAG,"PROBLEM!!!!");
					}
					else ServerDialog.executeHttpPostEntry(newEntry,address);
					/*else {
						ServerDialog mServerDialog = new ServerDialog(mode, mDba);
						try {
							mServerDialog.syncWithServer();
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}*/
				}
				else{
					if (newEntry.hasEnclosures()){
						mDba.insertEnclosure(mDba.mDb,enc , commentId,DataBaseSchema.ADHOC);
					}
				}

				cur.close();
			}
			return null;
		}


		protected void onPreExecute() {
			Log.d(TAG,"dialog");
		}

		protected void onPostExecute(Boolean result) {	
			Crouton.makeText(getActivity(), "Comment added",Style.INFO).show();
			getActivity().getSupportFragmentManager().beginTransaction()
			.replace(R.id.main_frame, new StreamFragment())
			.commit();
		}
	}
	
	public Drawable rescaleImage(String imagePath, int newWidth, int newHeight){
		try{
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds=true;

			//o.inSampleSize=4;

			if (imagePath!=null){
				BitmapFactory.decodeStream(new FileInputStream(imagePath),null,o);
			}

			//Find the correct scale value. It should be the power of 2.
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true){
				if(width_tmp/2<newWidth || height_tmp/2<newHeight)
					break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}

			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize=scale;

			// create a matrix for the manipulation
			Matrix matrix = new Matrix();
					// resize the bit map

			if(height_tmp<width_tmp)
				matrix.postRotate(90);

			// recreate the new Bitmap
			// make a Drawable from Bitmap to allow to set the BitMap
			// to the ImageView, ImageButton or what ever
			BitmapDrawable bmd = new BitmapDrawable(BitmapFactory.decodeStream(new FileInputStream(imagePath), null, o2));
			return bmd;

		} catch (FileNotFoundException e){
			Log.d(TAG,"File Not Found");
			return null;
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);  
		Log.d(TAG,"onActivityResult()");

		if (resultCode == getActivity().RESULT_OK){

			if(requestCode == CAMERA) {

				//Log.i(TAG, "CAMERA SONO NELL INTENT");
				File fi = new File("/sdcard/download/tmp");
				Uri u = null;

				try {
					u = Uri.parse(android.provider.MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), fi.getAbsolutePath(), null, null));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}

				if (!fi.delete()) {Log.i("From Camera intent", "Failed to delete " + fi);}
				Log.i(TAG, "CAMERA uri = " + u);
				cameraPhotoPath = getRealPathFromURI(u);
				Log.i(TAG, "CAMERA photo filepath = " + cameraPhotoPath);
				img.setVisibility(View.VISIBLE);

				img.setBackgroundDrawable(rescaleImage(cameraPhotoPath,img.getWidth(),img.getHeight()));

			}

			if (requestCode == SELECT_IMAGE)
				if (resultCode == Activity.RESULT_OK) {
					Uri selectedImage = data.getData();
					cameraPhotoPath = getRealPathFromURI(selectedImage);
					Log.i(TAG, "Gallery photo filepath = " + cameraPhotoPath);
					img.setVisibility(View.VISIBLE);

					img.setBackgroundDrawable(rescaleImage(cameraPhotoPath,img.getWidth(),img.getHeight()));
				} 
		}
	}
	
	public String getRealPathFromURI(Uri contentUri) {
		//String[] proj = { MediaStore.Images.Media.DATA};
		String[] proj = {};
		Cursor cursor = getActivity().managedQuery(contentUri, proj, null, null, null);

		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
		cursor.moveToFirst();

		return cursor.getString(column_index);
	}
}